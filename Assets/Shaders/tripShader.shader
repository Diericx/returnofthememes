﻿Shader "Custom/tripShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Blend SrcAlpha One
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		float3 Hue(float H)
		{
		    float R = abs(H * 6 - 3) - 1;
		    float G = 2 - abs(H * 6 - 2);
		    float B = 2 - abs(H * 6 - 4);
		    return saturate(float3(R,G,B));
		}

		float4 HSVtoRGB(in float3 HSV)
		{
		    return float4(((Hue(HSV.x) - 1) * HSV.y + 1) * HSV.z,1);
		}

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			float x = IN.uv_MainTex.x - 0.5;
			float y = IN.uv_MainTex.y - 0.5;
			float hue = sin(x*x + y*y - (_Time*10) ) + (_Time*2);
			hue = abs(fmod(hue,1));
			float3 output = HSVtoRGB( float3( hue,1,1 ) );
			o.Albedo = output;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}

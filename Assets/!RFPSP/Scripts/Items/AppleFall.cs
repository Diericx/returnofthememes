//AppleFall.cs by Azuline Studios© All Rights Reserved
//Script to make apples fall from trees if damaged or picked.
using UnityEngine;
using System.Collections;

public class AppleFall : MonoBehaviour {
	private Transform myTransform;

	void Start () {
		myTransform = transform;
	}
	
	public void ApplyDamage ( float damage ){
		if(!myTransform.rigidbody.useGravity){
			audio.pitch = Random.Range(0.75f * Time.timeScale, 1.0f * Time.timeScale);//add slight random value to sound pitch for variety
			audio.Play();//play "picking" sound effect
			myTransform.rigidbody.useGravity = true;	
		}
	}
	
	public void OnCollisionEnter(){
		if(!myTransform.rigidbody.useGravity){
			audio.pitch = Random.Range(0.75f * Time.timeScale, 1.0f * Time.timeScale);//add slight random value to sound pitch for variety
			audio.Play();//play "picking" sound effect
			myTransform.rigidbody.useGravity = true;	
		}
	}
	
}

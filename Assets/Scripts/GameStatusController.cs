﻿using UnityEngine;
using System.Collections;

public class GameStatusController : MonoBehaviour {

    public GameObject deathScreenCam;
    public GameObject mainCamera;
    public PlayerController pc;
    public spawn spawner;
    bool showingDeathScreen = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("p"))
        {
            switchToDeathScreen();
        }

        if (showingDeathScreen == true)
        {
            if (Input.GetKeyDown("f"))
            {
                switchToGameView();
            }
        }
	}

    public void switchToDeathScreen()
    {
        showingDeathScreen = true;
        deathScreenCam.tag = "MainCamera";
        mainCamera.tag = "Untagged";
        deathScreenCam.SetActive(true);
        mainCamera.SetActive(false);
        spawner.removeAllEnemies();
        spawner.shouldSpawn = false;
    }

    public void switchToGameView()
    {
        showingDeathScreen = false;
        mainCamera.tag = "MainCamera";
        deathScreenCam.tag = "Untagged";
        mainCamera.SetActive(true);
        deathScreenCam.SetActive(false);
        spawner.removeAllEnemies();
        spawner.shouldSpawn = true;
    } 
}

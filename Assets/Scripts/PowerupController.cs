﻿using UnityEngine;
using System.Collections;

public class PowerupController : MonoBehaviour {
    public GameObject enemyObj;
    private Texture[] powerupIcons;
    int powerup = 0;
    //wave vars
    float waveValue = 0;
    private Vector3 initLocalPos;

	// Use this for initialization
	void Start () {
        //set init position for wave
        initLocalPos = transform.localPosition;

        //get icon textures
        GameObject texHolder = GameObject.Find("PowerupTextureHolder");
        PowerupTextureHolder pth = texHolder.GetComponent<PowerupTextureHolder>();
        powerupIcons = pth.powerupIcons;

        //get what powerup enemy containes
        EnemyController ec = enemyObj.GetComponent<EnemyController>();
        powerup = ec.getPowerup();
        //print(ec.getPowerup());

        if (powerup == -1)
        {
            renderer.enabled = false;
        }
        else
        {
            
            renderer.enabled = true;

            //set texture accordingly
            gameObject.renderer.material.SetTexture("_MainTex", powerupIcons[powerup]);
        }

	}
	
	// Update is called once per frame
	void Update () {
        waveValue += 0.5f;
        float rad = waveValue * Mathf.Deg2Rad;
        float yToAdd = Mathf.Cos(rad)/10;
        transform.localPosition = new Vector3(initLocalPos.x, initLocalPos.y + yToAdd, initLocalPos.z);
	}
}

﻿using UnityEngine;
using System.Collections;

public class ObjectAnimator : MonoBehaviour {

    float SCALE_ANIM_SPEED = 1f;
    float ROT_ANIM_SPEED = 0.1f;
    int FONT_ANIM_SPEED = 1;

    private Vector3 initScale;
    private Vector3 initRotation;
    private int initFontSize;
    private GUIText textComponent;

	// Use this for initialization
	void Start () {
        initScale = transform.localScale;
        initRotation = transform.rotation.eulerAngles;

        if (transform.gameObject.GetComponent<GUIText>())
        {
            textComponent = gameObject.GetComponent<GUIText>();
            initFontSize = textComponent.fontSize;
        }
	}
	
	// Update is called once per frame
	void Update () {
        //xScale
        if (transform.localScale.x < initScale.x)
        {
            transform.localScale = new Vector3(transform.localScale.x + SCALE_ANIM_SPEED, transform.localScale.y, transform.localScale.z);
        }
        if (transform.localScale.x > initScale.x)
        {
            transform.localScale = new Vector3(transform.localScale.x - SCALE_ANIM_SPEED, transform.localScale.y, transform.localScale.z);
        }
        //yScale
        if (transform.localScale.y < initScale.y)
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y + SCALE_ANIM_SPEED, transform.localScale.z);
        }
        if (transform.localScale.y > initScale.y)
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y - SCALE_ANIM_SPEED, transform.localScale.z);
        }
        //xROTATION
        if (transform.rotation.eulerAngles.x < initRotation.x)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x + ROT_ANIM_SPEED, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }
        if (transform.rotation.eulerAngles.x > initRotation.x)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x - ROT_ANIM_SPEED, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }
        //yROTATION
        if (transform.rotation.eulerAngles.y < initRotation.y)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + ROT_ANIM_SPEED, transform.rotation.eulerAngles.z);
        }
        if (transform.rotation.eulerAngles.y > initRotation.y)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x - ROT_ANIM_SPEED, transform.rotation.eulerAngles.y - ROT_ANIM_SPEED, transform.rotation.eulerAngles.z);
        }
        //Font size
        if (textComponent.fontSize < initFontSize)
        {
            textComponent.fontSize += FONT_ANIM_SPEED;
        }
        if (textComponent.fontSize > initFontSize)
        {
            textComponent.fontSize -= FONT_ANIM_SPEED;
        }
	}
}

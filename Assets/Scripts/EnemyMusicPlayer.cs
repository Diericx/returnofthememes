﻿using UnityEngine;
using System.Collections;

public class EnemyMusicPlayer : MonoBehaviour {

    public AudioClip[] audioClips;
    private AudioSource audioSource;

    private GameObject musicPlayer;
    private AudioSource MPAudioSource;

	// Use this for initialization
	void Start () {
        musicPlayer = GameObject.Find("MusicPlayer");
        MPAudioSource = musicPlayer.GetComponent<AudioSource>();

        audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        //if the background music is playing, pause it when this music wants to start
        //print(audioSource.isPlaying);
        //if (audioSource.isPlaying == true)
        //{
        //    MPAudioSource.Pause();
        //}
        //else
        //{
        //    if (MPAudioSource.isPlaying == false)
        //    {
        //        MPAudioSource.Play();
        //    }
        //}
	}

    public void playSound(int id)
    {
        //if nothing else is playing then play the selected audio clip
        if (audioSource.isPlaying == false)
        {
            MPAudioSource.Pause();
            audioSource.clip = audioClips[id];
            audioSource.Play();
        }
    }

    public void stopSound()
    {
        audioSource.Pause();
        MPAudioSource.Play();
    }
}

﻿using UnityEngine;
using System.Collections;

public class SoundEffectPlayerController : MonoBehaviour {

    public AudioClip airHorn;
    public AudioClip[] randomClips;
    private AudioSource audioSource;
    public AudioSource airhornSource;


    // Use this for initialization
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void playAirhorn()
    {
        airhornSource.PlayOneShot(airHorn);
    }

    public void playSound()
    {
        int max = randomClips.Length;
        int rand = Random.Range(0, max);

        audioSource.clip = randomClips[rand];
        audioSource.Play();
    }

    public void stopSound()
    {
        audioSource.Pause();
    }
}

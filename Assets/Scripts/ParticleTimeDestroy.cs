﻿using UnityEngine;
using System.Collections;

public class ParticleTimeDestroy : MonoBehaviour {


    void Start()
    {
        if (particleSystem.loop)
        {
            Debug.LogError("Particle system can't self destruct if it loops");
        }
        else
        {
            StartCoroutine(SelfDestruct());
        }
    }

    private IEnumerator SelfDestruct()
    {
        yield return new WaitForSeconds(particleSystem.duration + 0.5f);
        GameObject.Destroy(transform.gameObject);
    }
}
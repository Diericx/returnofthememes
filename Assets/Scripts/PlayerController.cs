﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public GUIText moneyText;
    public GUIText scoreText;
    public GUIText ammoText;
    public GUIText timeText;
    public GUIText powerupText;
	public GameObject tripObj;
    public GameStatusController statusHandler;
    [HideInInspector]
    public bool stopWeaponFOV = false;

    private GameObject FPSWeaponsObj;
    private GameObject weaponObj;
    private GameObject soundEffectPlayer;
    private GameObject makeItRain;

    private PlayerWeapons playerWeaponsComponent;
    private FPSPlayer FPSPlayerScript;
    private EnemyMusicPlayer SEPController;

    //internally edited variables
    private float money = 0;
    private float score = 0;
    private int time = 0;
    private bool shouldAddToTime = true;
    public float reloadPowerupCooldown = 0f;
    public float slowTimePowerupCooldown = 0f;
    [HideInInspector]
    public bool reloadPowerupEnabled = false;
    [HideInInspector]
    public bool slowTimePowerupEnabled = false;
    //Item Costs
    private int PISTOL_COST = 200;
    private int MP5_COST = 1000;
    private int SHOTGUN_COST = 2500;
    private int SNIPER_COST = 4000;
    private int AK47_COST = 5000;

    //reload times
    float[] reloadTimes = { 0f, 1.25f, 2.35f, 2.35f, 2.5f, 2.35f, 2.2f, 0.6f, 2.5f };
    float[] reloadAnimSpd = { 0f, 1.25f, 1f, 1f, 1.25f, 0.7f, 1.15f, 0.75f, 1.25f };

    public AudioClip pickupSound;//sound to play when getting more ammo

	// Use this for initialization
	void Start () {
        FPSWeaponsObj = GameObject.Find("!!!FPS Weapons");
        soundEffectPlayer = GameObject.Find("EnemyMusicPlayer");
        makeItRain = GameObject.Find("MakeItRain");

        playerWeaponsComponent = FPSWeaponsObj.GetComponent<PlayerWeapons>();
        FPSPlayerScript = gameObject.GetComponent<FPSPlayer>();
        SEPController = soundEffectPlayer.GetComponent<EnemyMusicPlayer>();

        moneyText.pixelOffset = new Vector2(-Screen.width/2, Screen.height/2);
        scoreText.pixelOffset = new Vector2(Screen.width/2, Screen.height / 2);
        ammoText.pixelOffset = new Vector2(0, Screen.height / 2);
        timeText.pixelOffset = new Vector2(0, Screen.height / 2 - 50);
        powerupText.pixelOffset = new Vector2(0, (Screen.height / 2) - 300);

        StartCoroutine(everySecond());
	}
	
	// Update is called once per frame
	void Update () {
        //round money
        money =  Mathf.Round(money * 1000f) / 1000f;

        //update money and score text
        moneyText.text = "$" + money;
        scoreText.text = "Score: " + score;
        powerupText.text = "";

        //----------update cooldowns--------
        //Reload Time
        if (reloadPowerupCooldown > 0)
        {
            string roundedString = getRoundedString(reloadPowerupCooldown);
            //update text
            powerupText.text += "\nReload Speed for " + roundedString + " seconds!";
            reloadPowerupCooldown -= Time.deltaTime;
        }
        else if (reloadPowerupCooldown < 0)
        {
            reloadPowerupCooldown = 0;
            powerupText.text = "";
            if (reloadPowerupEnabled == true)
            {
                disableReloadPowerup();
                reloadPowerupEnabled = false;
            }
        }

        //Slow Speed
        if (slowTimePowerupCooldown > 0)
        {
            string roundedString = getRoundedString(slowTimePowerupCooldown);
            //update text
            powerupText.text += "\nSlow Time for " + roundedString + " seconds!";
            slowTimePowerupCooldown -= Time.deltaTime * (1/FPSPlayerScript.bulletTimeSpeed);
        }
        else if (slowTimePowerupCooldown < 0)
        {
            slowTimePowerupCooldown = 0;
            powerupText.text = "";
            if (slowTimePowerupEnabled == true)
            {
                disableSlowPowerup();
                slowTimePowerupEnabled = false;
            }
        }

        //set timer
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");
        timeText.text = minutes + ":" + seconds;

        if (Input.GetKeyDown("e")) {
            if (money >= 100)
            {
                buyAmmo();
                //play pickup sound
                if (pickupSound) { AudioSource.PlayClipAtPoint(pickupSound, transform.position, 0.75f); }
            }
        }

        if (Input.GetKeyDown("b"))
        {
            money += 1000;
        }


	}

    public void addMoney(int value, GameObject enemyShot)
    {
        if (value < 0)
        {
            EnemyController enemyController = enemyShot.GetComponent<EnemyController>();

            money += enemyController.moneyToGive;
        }
        else
        {
            money += value;
        }


        moneyText.GetComponent<GUIText>().fontSize = 84;//moneyText.GetComponent<GUIText>().fontSize * 2;
    }

    string getRoundedString(float input)
    {
        //get rounded value of reload time left
        float rounded = Mathf.Round(input * 100f) / 100f;
        string roundedString = rounded.ToString();
        //make sure it always has a zero at end
        roundedString = makeSureNumberHas2Decimals(roundedString);
        return roundedString;
    }

    public string makeSureNumberHas2Decimals(string input)
    {
        string result = input;
        int locationOfDec = input.LastIndexOf(".");
        if (locationOfDec == -1) //if there is no decimal
        {
            result = result + ".00";
        }
        else //if there is a decimal
        {
            string afterDec = input.Substring(locationOfDec);
            if (afterDec.Length < 3)//if there are no zeros after deiml
            {
                result = result + "0";
            }
        }

        return result;
    }

    public void addScore(int weapon)
    {
        score += weapon;

        scoreText.GetComponent<GUIText>().fontSize = 84;//scoreText.GetComponent<GUIText>().fontSize * 2;
    }

    public bool buyAmmo()
    {
        money -= 100;

        int weaponNumber = playerWeaponsComponent.getCurrentWeapon();

        PlayerWeapons PlayerWeaponsComponent = Camera.main.transform.GetComponent<CameraKick>().weaponObj.GetComponent<PlayerWeapons>();

        for (int i = 0; i < PlayerWeaponsComponent.weaponOrder.Length; i++)
        {
            if (PlayerWeaponsComponent.weaponOrder[i].GetComponent<WeaponBehavior>().weaponNumber == weaponNumber)
            {
                weaponObj = PlayerWeaponsComponent.weaponOrder[i];
                break;
            }
        }

        WeaponBehavior WeaponBehaviorComponent = weaponObj.GetComponent<WeaponBehavior>();

        WeaponBehaviorComponent.ammo += WeaponBehaviorComponent.bulletsPerClip * 4;

        return false;
    }

    //when a user buys a gun, check if they have enough money. BLOCK if not enough funds
    public bool buy(int gunId)
    {
        //print("GUN ID: " + gunId);
        bool result = false;
        if (gunId == 2)
        {
            if (money >= PISTOL_COST)
            {
                money = money - PISTOL_COST;
                result = true;
            }
        }
        else if (gunId == 4)
        {
            if (money >= MP5_COST)
            {
                money = money - MP5_COST;
                result = true;
            }
        }
        else if (gunId == 3)
        {
            if (money >= SHOTGUN_COST)
            {
                money = money - SHOTGUN_COST;
                result = true;
            }
        }
        else if (gunId == 7)
        {
            if (money >= SNIPER_COST)
            {
                money = money - SNIPER_COST;
                result = true;
            }
        }
        else if (gunId == 5)
        {
            if (money >= AK47_COST)
            {
                money = money - AK47_COST;
                result = true;
            }
        }

        return result;
    }

    public int getTime() {
        return time;
    }

    public float getMinutes()
    {
        float minutes = Mathf.Floor(time / 60);
        return minutes;
    }

    public void enableReloadPowerup()
    {
        //add to cooldown
        reloadPowerupCooldown += 5;
        //change bool
        reloadPowerupEnabled = true;

        //put reload effect into action
        int i = 0;
        foreach (Transform child in FPSWeaponsObj.transform)
        {
            print(child);
            WeaponBehavior wb = child.GetComponent<WeaponBehavior>();
            wb.reloadTime = reloadTimes[i]/2;
            wb.reloadAnimSpeed = reloadAnimSpd[i]*2;
            i++;
        }
    }

    public void enableSlowPowerup()
    {
        //add to cooldown
        slowTimePowerupCooldown += 5;
        //change bool
        slowTimePowerupEnabled = true;
		//enable trip animation
		tripObj.renderer.enabled = true;

        //put reload effect into action
        FPSPlayerScript.enableSlowTime();
    }

    public void disableSlowPowerup()
    {
        //add to cooldown
        slowTimePowerupCooldown = 0;
        //change bool
        slowTimePowerupEnabled = true;
        //stop powerup music and switch back to background music
        SEPController.stopSound();
		//disable trip anim
		//enable trip animation
		tripObj.renderer.enabled = false;
        //put reload effect into action
        FPSPlayerScript.disableSlowTime();
    }

    public void disableReloadPowerup()
    {
        reloadPowerupCooldown = 0; //2.35, 1.1
        //put reload effect into action
        int i = 0;
        foreach (Transform child in FPSWeaponsObj.transform)
        {
            print(child);
            WeaponBehavior wb = child.GetComponent<WeaponBehavior>();
            wb.reloadTime = reloadTimes[i];
            wb.reloadAnimSpeed = reloadAnimSpd[i];
            i++;
        }
    }

    public void startMoneyAnim()
    {
        StartCoroutine(showMoneyAnim());
    }
    
    public IEnumerator showMoneyAnim () {
        makeItRain.renderer.enabled = true;
        print("START");
        yield return new WaitForSeconds(2);
        makeItRain.renderer.enabled = false;
        print("STOP RAINING");
    }

    void OnTriggerEnter(Collider other)
    {
        statusHandler.switchToDeathScreen();
    }

    IEnumerator everySecond()
    {
        while (shouldAddToTime)
        {
            time += 1;
            yield return new WaitForSeconds(1);
        }
    }

    void OnGUI()
    {

    }
}

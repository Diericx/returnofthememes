﻿using UnityEngine;
using System.Collections;

public class RandomAirhorns : MonoBehaviour {

    int CHANCE = 20;
    float LENGTH = 2;
    float AIRHORN_SPD = 1005f;

    public bool stopWeaponFOV = false;

    public GameObject airhorn;
    public PlayerController pc;
    public GameObject soundEffectController;
    public SoundEffectPlayerController SEPController;

	// Use this for initialization
	void Start () {
        soundEffectController = GameObject.Find("SoundEffectPlayer");
        SEPController = soundEffectController.GetComponent<SoundEffectPlayerController>();
        StartCoroutine(every5Seconds());
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 rot = airhorn.transform.rotation.eulerAngles;
        airhorn.transform.rotation = Quaternion.Euler(new Vector3(rot.x, rot.y, rot.z + (AIRHORN_SPD * Time.deltaTime)));
	}

    IEnumerator every5Seconds()
    {
        while (true)
        {
            int rand = Random.Range(1, 100);
            if (rand >= 0 && rand <= CHANCE)
            {
                if (pc.reloadPowerupEnabled == false & pc.slowTimePowerupEnabled == false)
                {
                    StartCoroutine(fuckWithFOV());
                }
            }
            yield return new WaitForSeconds(5);
        }
    }

    public IEnumerator fuckWithFOV()
    {
        SEPController.playAirhorn();
        airhorn.renderer.enabled = true;
        stopWeaponFOV = true;
        int degree = 0;
        float cooldown = LENGTH;
        while (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            Camera.main.fieldOfView = 75 + 20; //(((Mathf.Sin(rads) * 50) + 50 / 2));
            yield return new WaitForSeconds(0.0001f);
        }
        stopWeaponFOV = false;
        airhorn.renderer.enabled = false;

    }
}

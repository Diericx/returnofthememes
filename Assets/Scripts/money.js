﻿#pragma strict

//we use these 8 textures to animate the pickup object
var quk1:Texture;
var quk2:Texture;
var quk3:Texture;
var quk4:Texture;
var quk5:Texture;
var quk6:Texture;
var quk7:Texture;
private var counter:float = 0.0;

function Start () {
Time.timeScale = 1;
renderer.enabled = false;
//transform.Rotate(Vector3(0,180,0));
//transform.Translate(Vector3(-5,5,0));
    //Time.timeScale = 1.5;
}

function Update () {
//we use this counter to keep track of time for animating the pickup
counter += Time.deltaTime*30;

if(counter > 0 && counter < 1 && renderer.material.mainTexture != quk1){
	renderer.material.mainTexture = quk1;
}

if(counter > 1 && counter < 2 && renderer.material.mainTexture != quk2){
	renderer.material.mainTexture = quk2;
}
if(counter > 2 && counter < 3 && renderer.material.mainTexture != quk3){
	renderer.material.mainTexture = quk3;
}
if(counter > 3 && counter < 4 && renderer.material.mainTexture != quk4){
	renderer.material.mainTexture = quk4;
}
if(counter > 4 && counter < 5 && renderer.material.mainTexture != quk5){
	renderer.material.mainTexture = quk5;
}
if(counter > 5 && counter < 6 && renderer.material.mainTexture != quk6){
	renderer.material.mainTexture = quk6;
}
if(counter > 6 && counter < 7 && renderer.material.mainTexture != quk7){
	renderer.material.mainTexture = quk7;
}

if(counter > 7){
counter = 0.0;
}

}



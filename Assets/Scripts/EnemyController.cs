﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public float localX;

    public float maxHealth = 100;
    private int powerup = 0;
    private int maxPowerupID = 2;

    float health = 100;

    public float moneyToGive = 0;

	private GameObject player;
    
    public GameObject healthBar;
    private GameObject enemyMusicPlayer;
    private GameObject soundEffectPlayer;
    private GameObject[] particleEmitters;

    private PlayerController playerController;
    private EnemyMusicPlayer EMPController;
    private SoundEffectPlayerController SEPController;

	public float ENEMY_SPEED = 2f;
    int POWERUP_CHANCE = 40;

	// Use this for initialization
	void Start () {
        health = maxHealth;
        player = GameObject.Find("!!!FPS Player");
        enemyMusicPlayer = GameObject.Find("EnemyMusicPlayer");
        soundEffectPlayer = GameObject.Find("SoundEffectPlayer");

        playerController = player.GetComponent<PlayerController>();
        EMPController = enemyMusicPlayer.GetComponent<EnemyMusicPlayer>();
        SEPController = soundEffectPlayer.GetComponent<SoundEffectPlayerController>();

        //load particle emitters
        GameObject particleEmitterHolder = GameObject.Find("ParticleEmitterHolder");
        ParticleEmittersHolder peh = particleEmitterHolder.GetComponent<ParticleEmittersHolder>();
        particleEmitters = peh.particleEmitters;

        //randommize powerup
        int rand = Random.Range(1, 100);
        if (rand >= 0 && rand <= POWERUP_CHANCE)
        {
            powerup = Random.Range(0, maxPowerupID + 1);
        }
        else
        {
            powerup = -1;
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(player.transform);
		transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x + 180, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z + 180);
		transform.Translate(Vector3.forward * Time.deltaTime * ENEMY_SPEED * -1);

        //if the enemy is close to the player shake the enemy
        float distanceFromPlayer = Vector3.Distance(transform.position, player.transform.position);
        if (distanceFromPlayer <= 20)
        {
            int angle = Time.frameCount;
            float rad = angle * Mathf.Deg2Rad;
            float x = Mathf.Cos(rad) * 0.01f;
            Vector3 initPos = transform.position;
            localX = initPos.x;
            //Vector3 newPos = new Vector3(initPos.x + x, initPos.y, initPos.z);
            transform.position += transform.right * x;
        }


        if (health <= 0)
        {
            killSelf();
        }

        updateHealthBar();
	}

    public void killSelf()
    {
        if (powerup == 0) // Reload powerup
        {
            playerController.enableReloadPowerup();
        }
        else if (powerup == 1) // slow time powerup
        {
            playerController.enableSlowPowerup();
            EMPController.playSound(0);
        }
        else if (powerup == 2)
        {
            playerController.startMoneyAnim();
            playerController.addMoney(1000, null);

        }

        SEPController.playSound();
        playerController.addScore(100);
        displayParticles();
        GameObject.Destroy(transform.parent.gameObject);
        GameObject.Destroy(gameObject);
    }

    void displayParticles()
    {
        foreach (GameObject g in particleEmitters)
        {
            GameObject newPartSystem = (GameObject)Instantiate(g, transform.position, Quaternion.identity);
        }
    }

    public void updateHealthBar()
    {

        healthBar.transform.localScale = new Vector3(getHealthPercent() * 1, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
        healthBar.transform.localPosition = new Vector3((-1f + healthBar.transform.localScale.x)/2, 0.637493f, 0);

        
    }

    public int getPowerup()
    {
        return powerup;
    }

    public float getHealthPercent()
    {
        return (float)(health / maxHealth);
    }

    public void applyDamage(float damage)
    {
        health = health - damage;
    }
}

﻿using UnityEngine;
using System.Collections;

public class spawn : MonoBehaviour {

	float RADIUS = 75f;
	float SPAWN_RATE = 1f;

    public GameObject[] enemyPrefabs;

    ArrayList enemies = new ArrayList();

    private PlayerController playerController;

    int enemiesAbleToSpawn = 0; //max index that will be used to get a prefab from the enemies array

    public bool shouldSpawn = true;

	bool isAlive = true;
	// Use this for initialization
	void Start () {
		StartCoroutine (spawnEnemy ());

        playerController = transform.gameObject.GetComponent<PlayerController>();

		var player = transform.GetComponent<FPSPlayer>(); //.hitPoints = 10;
	}
	
	// Update is called once per frame
	void Update () {
        //Update spawn rate
        if (playerController.getMinutes() == 1)
        {
            SPAWN_RATE = 3f;
        }
        else if (playerController.getMinutes() == 2)
        {
            SPAWN_RATE = 1f;
        }
        //Update sprites to spawn
        if (playerController.getTime() == 30)
        {
            enemiesAbleToSpawn = 1;
        }
        else if (playerController.getTime() == 60)
        {
            enemiesAbleToSpawn = 2;
        }
	}

    public void removeAllEnemies()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null)
            {
                GameObject.Destroy((GameObject)enemies[i]);
            }
        }
        enemies = new ArrayList();
    }

    int randomEnemy()
    {
        int enemyID = Random.Range(0, enemiesAbleToSpawn + 1);
        return enemyID;
    }

	IEnumerator spawnEnemy(){
		while (isAlive){
            if (shouldSpawn)
            {
                float angle = Random.Range(1, 360);
                float rad = angle * Mathf.Deg2Rad;

                float x = RADIUS * Mathf.Cos(rad);
                float z = RADIUS * Mathf.Sin(rad);

                GameObject newEnemy = (GameObject)Instantiate(enemyPrefabs[randomEnemy()], new Vector3(x + transform.position.x, transform.position.y, z + transform.position.z), Quaternion.identity);
                enemies.Add(newEnemy);
            }
            yield return new WaitForSeconds(SPAWN_RATE);
		}
	}

}

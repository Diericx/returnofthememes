﻿using UnityEngine;
using System.Collections;

public class DeathScreenHandler : MonoBehaviour
{

    public GUIText message;
    public GUIText restartText;
    public GameObject image;

    float IMAGE_GROWTH_RATE = 0.002f;
    WiiUGamePad myGP = WiiUInput.GetGamePad();

	// Use this for initialization
	void Start () {
        message.pixelOffset = new Vector2(0, Screen.height / 2);
        restartText.pixelOffset = new Vector2(0, -Screen.height / 2);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("space"))
        {
            Application.LoadLevel("mainScene"); 
        }

        Vector3 scale = image.transform.localScale;

        image.transform.localScale = new Vector3(scale.x + IMAGE_GROWTH_RATE, scale.y + (IMAGE_GROWTH_RATE*0.6f), 1);
	
	}
}
